One Observatory Park is an exclusive development nestled in the heart of one of South Denver's most sought-after neighborhoods, offering enhanced studio, one- and two-bedroom apartment homes. The building features an impressive eleven stories of luxury living opportunities.

Address: 2360 E Evans Avenue, Denver, CO 80210, USA

Phone: 303-733-3667